%Hello Marko 
% Clear workspace and close figures
delete(gcp('nocreate'))
close all;
clc;
clear;

% Start the parallel pool
parpool;

%% Define rocket constants and states
g = 9.81; % Earth's gravitational acceleration
Mass = 3e5; % Rocket mass in kg
MaxThrust = 854e6/9/5; % Max main thrust in Newtons
I = 1/3 * Mass * 70^2; % Rotational inertia
MaxRCSThrust = 1e4; % Maximum RCS thrust in N*m
Cd = 0.8; % Drag coefficient
A = pi * (5^2); % Cross-sectional area assuming a diameter of 10m
rho0 = 1.225 * 5; % Air density at sea level in kg/m^3
H = 8500; % Scale height in m

% Initial and final states: [x, y, vx, vy, theta, omega]
initialStates = [0, 80e3, 3200, 170, tan(3200/170), 0];
desiredState = [350e3, 0, 0, 0, pi/2, 0]; % Land at starting position, upright

%% Create initial spline for throttle and RCS
timeNodes = linspace(0, 600, 10);
initialThrottleProfile = linspace(1, 0, numel(timeNodes));
initialRCSProfile = zeros(size(timeNodes)); % Start with no RCS thrust

%% Optimization using fmincon
% Optimization options with adjusted tolerances
options = optimoptions('fmincon', 'Algorithm', 'interior-point', 'UseParallel', true, ...
                       'TolFun', 1e-6, 'TolX', 1e-6, 'TolCon', 1e-6);

% Optimization bounds
lb = [-ones(size(initialThrottleProfile)), -ones(size(initialRCSProfile))];
ub = [ones(size(initialThrottleProfile)), ones(size(initialRCSProfile))];


% Assuming throttle changes by at most 1 unit over 1 second
maxThrottleChangeRate = 0.1; % You can adjust this value based on your requirements

optimalControls = fmincon(@(profile) objectiveFunction(profile, timeNodes, initialStates, desiredState, g, Mass, MaxThrust, Cd, A, rho0, H, MaxRCSThrust, I), ...
                          [initialThrottleProfile, initialRCSProfile], ...
                          [], [], [], [], lb, ub, @(profile) throttleConstraint(profile, timeNodes, maxThrottleChangeRate), options);


optimalThrottle = optimalControls(1:numel(timeNodes));
optimalRCS = optimalControls(numel(timeNodes) + 1:end);

%% Simulate with optimal throttle and RCS
throttleFun = @(t) interp1(timeNodes, optimalThrottle, t, 'pchip', 'extrap');
RCSFun = @(t) max(-1, min(1, interp1(timeNodes, optimalRCS, t, 'pchip', 'extrap')));

[t, y] = ode45(@(t,y) rocketDynamics(t, y, throttleFun(t), RCSFun(t), g, Mass, MaxThrust, Cd, A, rho0, H, MaxRCSThrust, I), [0, timeNodes(end)], initialStates);

%% Plot trajectory
figure;
hold on;


% Plot trajectory colored by speed
speeds = sqrt(y(:,3).^2 + y(:,4).^2);
for i = 1:length(t) - 1
    line(y(i:i+1,1), y(i:i+1,2), 'Color', getColorBasedOnSpeed(speeds(i)), 'LineWidth', 1.5);
end

% Compute arc-length segments of the trajectory
arcLengths = computeArcLengths(y);

% Add thrust vectors along the trajectory based on equal arc-length segments
numVectors = 50; 
vectorSpacing = arcLengths(end) / numVectors;

vectorIdx = [];
currentDist = 0;

for i = 1:length(arcLengths) - 1
    if arcLengths(i) >= currentDist
        vectorIdx(end+1) = i;
        currentDist = currentDist + vectorSpacing;
    end
end

% Increase the thrust vector length multiplier
thrustVectorMultiplier = 5000;  % Increased value for longer thrust vectors

for i = vectorIdx
    theta_current = y(i,5);
    thrustMagnitude = throttleFun(t(i)) * MaxThrust; % Thrust magnitude at this time point
    normalizedThrustLength = (thrustMagnitude / MaxThrust) * thrustVectorMultiplier; % Normalize

    thrustDir = [cos(theta_current), sin(theta_current)]; % Thrust direction based on orientation
    startPt = [y(i,1), y(i,2)];
    endPt = startPt + normalizedThrustLength * thrustDir; 

    currentSpeed = sqrt(y(i,3)^2 + y(i,4)^2);
    quiver(startPt(1), startPt(2), endPt(1) - startPt(1), endPt(2) - startPt(2), 0, ...
           'Color', getColorBasedOnSpeed(currentSpeed), ...
           'LineWidth', 1.5, 'MaxHeadSize', 0.05, 'AutoScale', 'off');
end

% Add a "landing pad" centered at the target end point
padLength = 15000; % 5 km length for the landing pad for visual representation
plot([desiredState(1) - padLength/2, desiredState(1) + padLength/2], [desiredState(2), desiredState(2)], 'k-', 'LineWidth', 5.5);

% Plot setup
colorbar;
xlabel('Horizontal Position (m)');
ylabel('Altitude (m)');
title('Rocket Trajectory Colored by Speed and Atmospheric Gradient');
grid on;
hold off;


%% Additional function to determine color based on speed
function color = getColorBasedOnSpeed(speed)
    cmap = jet(256);
    minSpeed = 0;
    maxSpeed = 4000;
    idx = round(1 + (255 * (speed - minSpeed) / (maxSpeed - minSpeed)));
    idx = max(1, min(idx, 256)); % Ensure idx is between 1 and 256
    color = cmap(idx, :);
end

function dy = rocketDynamics(~, y, throttle, RCS, g, Mass, MaxThrust, Cd, A, rho0, H, MaxRCSThrust, I)
    % Unpack states
    x = y(1);
    h = y(2);
    vx = y(3);
    vy = y(4);
    theta = y(5);
    omega = y(6);
    
    % Air density as a function of altitude
    rho = rho0 * exp(-h/H);
    
    % Aerodynamic drag
    V = sqrt(vx^2 + vy^2);
    Fdrag = 0.5 * rho * V^2 * Cd * A;
    Fdrag_x = Fdrag * vx/V;
    Fdrag_y = Fdrag * vy/V;
    
    % Forces
    Fx = throttle * MaxThrust * cos(theta) - Fdrag_x;
    Fy = throttle * MaxThrust * sin(theta) - Mass * g - Fdrag_y;
    
    % Introduce torque due to RCS
    tau = RCS * MaxRCSThrust;
    

    % State derivatives
    dx = vx;
    dy = vy;
    dvx = Fx/Mass;
    dvy = Fy/Mass;
    dtheta = omega;
    domega = tau/I;
    
    dy = [dx; dy; dvx; dvy; dtheta; domega];
end

function cost = objectiveFunction(profile, timeNodes, initialState, desiredState, g, Mass, MaxThrust, Cd, A, rho0, H, MaxRCSThrust, I)
    throttleProfile = profile(1:numel(timeNodes));
    RCSProfile = profile(numel(timeNodes)+1:end);

    throttleFun = @(t) interp1(timeNodes, throttleProfile, t, 'pchip', 'extrap');
    RCSFun = @(t) max(-1, min(1, interp1(timeNodes, RCSProfile, t, 'pchip', 'extrap')));

    [t, y] = ode45(@(t,y) rocketDynamics(t, y, throttleFun(t), RCSFun(t), g, Mass, MaxThrust, Cd, A, rho0, H, MaxRCSThrust, I), [0, timeNodes(end)], initialState);

    stateDiff = norm(y(end,:) - desiredState);
    statePenalty = 1e4 * stateDiff;
    
    undergroundValues = max(0, -y(:,2)); % Only take the negative altitudes, setting positive values to zero
    undergroundIntegral = trapz(t, undergroundValues); % Compute the integral using the trapezoidal method
    undergroundPenalty = 1e5 * undergroundIntegral; % Penalty based on integral value

    fuelUsage = (trapz(timeNodes, throttleProfile)); % Integral of throttle profile over the time nodes
    fuelPenalty = 1e5 * fuelUsage; % Fuel usage penalty

    cost = statePenalty + undergroundPenalty + fuelPenalty; % Cost includes state difference, underground penalty, and fuel usage
end

function [c, ceq] = throttleConstraint(profile, timeNodes, maxThrottleChangeRate)
    throttle = profile(1:numel(timeNodes));
    deltaTime = mean(diff(timeNodes)); % Assuming even spacing of timeNodes
    maxThrottleChange = maxThrottleChangeRate * deltaTime;
    
    throttleChange = diff(throttle);
    
    % Constraints: 
    % The throttle change should be within [-maxThrottleChange, maxThrottleChange]
    c(1:numel(throttle) - 1) = abs(throttleChange) - maxThrottleChange;
    
    % No equality constraints in this case
    ceq = [];
end


function arcLengths = computeArcLengths(y)
    % Initialize arc-lengths array
    arcLengths = zeros(size(y,1),1);

    % Calculate the arc-lengths
    for i = 2:length(arcLengths)
        dx = y(i,1) - y(i-1,1);
        dy = y(i,2) - y(i-1,2);
        segmentLength = sqrt(dx^2 + dy^2);
        arcLengths(i) = arcLengths(i-1) + segmentLength;
    end
end
